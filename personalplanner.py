# Check for and adjust month input if necessary
def validateMonth(month):
    if (month < 1 or month > 12):
        month = 1
        return month
    else:
        return month
    

# Check for and adjust day input if necessary
# (don't forget about leap year)

def validateDay(month, day, year):
    isLeapYear = 0
    if (year % 400 == 0):
        isLeapYear = 1
    elif (year % 100 == 0):
        isLeapYear = 0
    elif (year % 4 == 0):
        isLeapYear = 1
    else:
        isLeapYear = 0
    if (month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12):
        if (day < 1 or day > 31):
            return 1
        else:
            return day
    if (month == 4 or month == 6 or month == 9 or month == 11):
        if (day < 1 or day > 30):
            return 1
        else:
            return day
    if (month == 2 and isLeapYear == 1):
        if (day < 1 or day > 29):
            return 1
        else:
            return day
    if (month == 2):
        if (day < 1 or day > 28):
            return 1
        else:
            return day
    
        

# This function is used to print all events to the user in the format
# Event
# Date: Month Day, Year

def printEvents():
    arMonths = ['monthholder', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', ' September', 'October', 'November', 'December']

    print("\n******************** List of Events ********************")
    for i in range(0, len(eventName)):
        print(eventName[i])
        print("Date: " + arMonths[eventMonth[i]] + " " + str(eventDay[i]) + ", " + str(eventYear[i]))



# This function is used to prompt, adjust and 
# append values to the 4 parallel arrays
def addEvent():
    addAnotherEvent = ""
    while(addAnotherEvent != "NO"):
        addName = input("What is the event: ")
        addMonth = int(input("What is the month (number): "))
        addDay = int(input("What is the date: "))
        addYear = int(input("What is the year: "))
        eventName.append(addName)
        eventMonth.append(validateMonth(addMonth))
        eventDay.append(validateDay(addMonth, addDay, addYear))
        eventYear.append(addYear)
        addAnotherEvent = input("Do you want to enter another event? NO to stop: ")
    printEvents()
                

    
#*********** MAIN **********
eventName = []
eventMonth = []
eventDay = []
eventYear = []

addEvent()


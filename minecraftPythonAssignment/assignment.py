from mcpi.minecraft import Minecraft
from mcpi import block

def init():
    ip = "127.0.0.1"
    mc = Minecraft.create(ip, 4711)
    #x, y, z = mc.player.getPos()
    return mc


def clearAir(mc, x, y, z):
    mc.setBlocks(x-100,y-30,z-100,x+100,y+100,z+100,0)


def fifteenBlocks(mc, x, y, z):
    x = x+10
    y = y+5
    count = 0
    for i in range(0, 15):
        mc.setBlock(x,y + count,z,35,i)
        mc.setBlock(x + count,y,z,35,i)
        mc.setBlock(x,y,z + count,35,i)
        count = count + 1
    
def assignment():
    mc=init()
    x, y, z = mc.player.getPos()
    clearAir(mc, x, y, z)
    fifteenBlocks(mc, x, y, z)
    mc.postToChat("X Y Z")


import tkinter as tk
from staticAmongUsBlock import *
from assignment import *


root = tk.Tk()
root.title("MCPI Loader")
root.minsize(500, 500)

amongusBtn = tk.Button(root, text="AmongUs",command=amongus)
amongusBtn.pack()

assignmentBtn = tk.Button(root, text="Assignment XYZ",command=assignment)
assignmentBtn.pack()

def main():
    root.mainloop()

if __name__ == '__main__':
    main()


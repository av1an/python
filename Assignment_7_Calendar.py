#leap_year

def leap_year(y):
    if (y % 400 == 0):
        return 1
    elif (y % 100 == 0):
        return 0
    elif (y % 4 == 0):
        return 1
    else:
        return 0
    return y
    
#number_of_days

def number_of_days(m, y):
    y = leap_year(y)
    if (m == 1 or m == 3 or m == 5 or m == 7 or m == 8 or m == 10 or m == 12):
        return 31
    if (m == 4 or m == 6 or m == 9 or m == 11):
        return 30
    if (m == 2 and y == 1):
        return 29
    elif (m == 2 and y == 0):
        return 28

#days_left

def days_left(d, m, y):
    sum = 0
    for i in range(m, 13):
       sum = sum + number_of_days(m, y)
       m = m + 1
    return sum - d

#main

print("Please enter a date")

day = int(input("Day: "))
if (day < 1 or day > 31):
    print("Make sure day is between 1 or 31")
    end=" "
month = int(input("Month: "))

if (month < 1 or month > 12):
    print("Make sure month is between 1 or 12")
    end=" "
year = int(input("Year: "))

if (year < 0):
    print("Can't be less than 0")
    end=" "
    
print("Menu:\n1) Calculate the number of days in the given month.\n2) Calculate the number of days left in the given year.")

monOrYear = int(input(""))

if (monOrYear == 1):
        print(number_of_days(month, year))
elif (monOrYear == 2):
        print(days_left(day, month, year))
else:
    print("Invalid selection pick 1 or 2")
    
    
#Getting Errors on Test 3 , 7 , 8 , and 9 All problems that I fixed in the code
    

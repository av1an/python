import turtle
import math

min_branch_len = 5

def buildtree(t, branch_length, shorten_by, angle):
	if branch_length > min_branch_len:
		t.fd(branch_length)
		new_length = branch_length - shorten_by

		t.lt(angle)
		buildtree(t, new_length, shorten_by, angle)

		t.rt(angle * 2)
		buildtree(t, new_length, shorten_by, angle)

		t.lt(angle)
		t.backward(branch_length)

turtle.bgcolor("#c2c2c2")
tree = turtle.Turtle()
tree.speed(10000)
tree.color("#ca0000")
tree.ht()	
turtle.tracer(0,0)

tree.fd(175)
buildtree(tree, 50, 5, 30)
turtle.penup
tree.goto(0, 0)
turtle.pendown
tree.lt(90)
tree.color("#54a630")
buildtree(tree, 50, 5, 30)
turtle.penup
tree.goto(0,0)
turtle.pendown
tree.lt(180)
tree.color("blue")
buildtree(tree, 50, 5, 30)
turtle.penup
tree.goto(0,0)
turtle.pendown
tree.lt(270)
tree.color("yellow")
tree.fd(175)
buildtree(tree, 50, 5, 30)
		
turtle.mainloop()

if __name__ == "__main__":
	main()


	


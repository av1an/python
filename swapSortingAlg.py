
    #Sorting alg

def sortAlg(ar):
    for i in range(0, len(ar)):
        for j in range(i+1, len(ar)):
            if(ar[i][0] > ar[j][0]):
                ar[i], ar[j] = ar[j], ar[i]
 
#MAIN

vocab = ["Libraries", "Bandwidth", "Hierarchy", "Software", "Firewall", "Cybersecurity","Phishing", "Logic", "Productivity"]

print(vocab)
sortAlg(vocab)
print(vocab)